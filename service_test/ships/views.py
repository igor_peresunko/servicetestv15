from rest_framework import viewsets, status
from rest_framework.response import Response

from models import Deck
from parsing_names import DeckNameParser
from serializers import DecksSerializer


class DeckViewSet(viewsets.ViewSet):
    """
    Returns an object from the model upon request
    """
    def list(self, request):
        obj = DeckNameParser()
        search_options = request.GET
        messege = """
        Valid request: api/v1/match-deck?name=<deck name>&ship=<ship name>
        """

        if not search_options.get('name') or not search_options.get('ship'):
            return Response(messege.strip(), status=status.HTTP_400_BAD_REQUEST)
        else:
            names = obj.parse(search_options['name'])
            for index, i in enumerate(names):
                if index < 1:
                    queryset = Deck.objects.filter(name__icontains=i,
                                                   ship=search_options['ship'])
                else:
                    queryset = queryset.filter(name__icontains=i,
                                               ship=search_options['ship'])
                if i in ['-', 'na']:
                    queryset = Deck.objects.filter(name='Other',
                                                   ship=search_options['ship'])

            serializer = DecksSerializer(queryset, many=True)
            return Response(serializer.data)
