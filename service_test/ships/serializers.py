from rest_framework import serializers

from models import Deck


class DecksSerializer(serializers.ModelSerializer):
    """
    Serializes the Deck model
    """
    description = serializers.CharField(source='ship.description')

    class Meta:
        model = Deck
        fields = ('id', 'name', 'sort_order', 'description')
