from __future__ import unicode_literals

from django.db import models


class Ship(models.Model):
    name = models.CharField(max_length=100)
    wieght = models.FloatField()
    capacity = models.IntegerField()
    year = models.DateField()
    description = models.TextField(blank=True)


class Deck(models.Model):
    name = models.CharField(max_length=100)
    sort_order = models.IntegerField()
    ship = models.ForeignKey('Ship')
    image_link = models.URLField(blank=True)
