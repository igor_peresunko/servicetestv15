from django.contrib import admin
from models import Ship, Deck


@admin.register(Ship)
class ShipAdmin(admin.ModelAdmin):
    list_display = ('name', 'wieght', 'capacity', 'year', 'description')


@admin.register(Deck)
class DeckAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort_order', 'ship', 'image_link')
