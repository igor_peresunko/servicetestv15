class DeckNameParser(object):
    """
    Parses deck name into appropriate format
    """
    def __init__(self):
        self.numbs = {
            'one': 1,
            'two': 2,
            'three': 3,
            'four': 4,
            'five': 5,
            'six': 6,
            'seven': 7,
            'eight': 8,
            'nine': 9,
            'ten': 10,
            'eleven': 11,
            'twelve': 12,
            'thirteen': 13,
            'fourteen': 14,
            'fifteen': 15,
            'sixteen': 16,
            'seventeen': 17,
            'eighteen': 18,
            'nineteen': 19,
            'twenty': 20,
        }
        self.result = []
        self.result_value = 0

    def parse(self, parsed_line):
        """
        Split the sequence into separate words
        and then translating them to numbers if they are in the dictionary
        :param parsed_line: input string to parse - Deck five
        :return: list of names - ['deck', 5]
        """

        def _proceed_line(line):
            for word in parsed_line.split():
                word = word.lower()
                if _word_is_number(word):
                    self.result_value += self.numbs[word]
                else:
                    self.result.append(word)

        def _add_result_value():
            if self.result_value:
                self.result.append(self.result_value)

        def _word_is_number(word):
            return word in self.numbs

        _proceed_line(parsed_line)
        _add_result_value()
        return self.result

if __name__ == '__main__':
    test_strings = ['twenty one', 'Deck five 2',
                    'Atlantic Deck', 'Other', 'NA',
                    '-']

    for name in test_strings:
        obj = DeckNameParser()
        print obj.parse(name)
