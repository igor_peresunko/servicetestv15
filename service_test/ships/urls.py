from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from views import DeckViewSet

router = DefaultRouter()
router.register(r'match-deck', viewset=DeckViewSet, base_name='match-deck')

urlpatterns = [
    url(r'^v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework'))
]
