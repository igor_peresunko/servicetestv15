from django.test import TestCase

from parsing_names import DeckNameParser


class TestingParse(TestCase):
    """Testing the function of translating words into numbers"""
    def setUp(self):
        self.obj = DeckNameParser()
        self.test_string_1 = ['Deck FIVE']
        self.test_string_2 = {'Deck': 'five'}
        self.test_string_3 = ('Deck FIVE', 'five')

    def test_only_name(self):
        """Check the parse if the string has no numbers"""
        parse_obj = DeckNameParser()
        self.assertEqual(['atlantic', 'deck'], parse_obj.parse('Atlantic Deck'))

    def test_name_and_numb(self):
        """Check parse name and the number in the string"""
        parse_obj = DeckNameParser()
        self.assertEqual(['acquamarina', 21], parse_obj.parse('Acquamarina twenty one'))

    def test_upper_register_numb(self):
        """Check the parser if the number in the upper register"""
        parse_obj = DeckNameParser()
        self.assertEqual(['deck', 5], parse_obj.parse('Deck FIVE'))

    def test_exceptions(self):
        """Check the parser on the transmission parameters of different types"""
        self.assertRaises(AttributeError, self.obj.parse, self.test_string_1)
        self.assertRaises(AttributeError, self.obj.parse, self.test_string_2)
        self.assertRaises(AttributeError, self.obj.parse, self.test_string_3)
