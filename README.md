1. Create a folder on the local drive and clone the repository ```https://igor_peresunko@bitbucket.org/igor_peresunko/servicetestv15.git```
2. Create a a virtual environment: ```virtualenv .env```
3. Run virtual environment ```. .env/bin/activate```
4. Install all packages from file __requirements.txt__ ```pip install -r requirements.txt```
5. Add the secret key into environment ```export SECRET_KEY='f^e%x02accm^0%p-6rs!$1*=u12%mq^&a#dy^hga6ulq!6g%_)'```
6. Navigate to the folder *../service_test*
7. Run the command ```python manage.py migrate```
8. Create a superuser ```python manage.py createsuperuser```
9. Start the server and go to the link http://127.0.0.1:8000/
